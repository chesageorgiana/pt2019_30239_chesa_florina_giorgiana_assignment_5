package model;

public class MeatItem extends MenuItem {

    public MeatItem(String name, Double price) {
        super(name, price);
        this.setType("Meat/Animal");
    }
}
