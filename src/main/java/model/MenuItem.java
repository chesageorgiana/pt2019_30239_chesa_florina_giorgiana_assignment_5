package model;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {

    private String id;
    private String name;
    private String type;
    private Double price;
    private Integer quantity;

    public MenuItem(String name, Double price) {
        this.id = "Item-" + RandomStringUtils.randomNumeric(8);
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuItem menuItem = (MenuItem) o;

        if (name != null ? !name.equals(menuItem.name) : menuItem.name != null) return false;
        if (type != null ? !type.equals(menuItem.type) : menuItem.type != null) return false;
        if (price != null ? !price.equals(menuItem.price) : menuItem.price != null) return false;
        return quantity != null ? quantity.equals(menuItem.quantity) : menuItem.quantity == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        return result;
    }
}
