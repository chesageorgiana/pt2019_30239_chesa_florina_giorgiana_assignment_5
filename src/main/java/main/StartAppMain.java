package main;


import graphicUserInterface.GuiFrame;
import model.MeatItem;
import model.MenuItem;
import model.Order;
import model.VeggieItem;
import restaurant.Restaurant;
import serializable.SerializableOperations;

import java.util.Arrays;

public class StartAppMain {

    public static void main(String[] args) {
        MenuItem a1= new VeggieItem("Castraveti murati",23.0);
        MenuItem a2= new VeggieItem("Rosi Cheri",12.0);
        MenuItem a3= new VeggieItem("Salata de varza",8.8);
        MenuItem a4= new MeatItem("Piept de pui", 10.8);
        MenuItem a5= new MeatItem("Piept de curcan", 10.8);
        MenuItem a6= new MeatItem("Friptura de vita", 10.8);
        MenuItem a7= new MeatItem("Porc la cuptor", 22.22);
        a1.setQuantity(20);
        a2.setQuantity(24);
        a3.setQuantity(52);
        a4.setQuantity(55);
        a5.setQuantity(123);
        a6.setQuantity(111);
        a7.setQuantity(15);
        Restaurant r = new Restaurant();
        r.addNewMenuItem(a1);
        r.addNewMenuItem(a2);
        r.addNewMenuItem(a3);
        r.addNewMenuItem(a4);
        r.addNewMenuItem(a5);
        r.addNewMenuItem(a6);
        r.addNewMenuItem(a7);
        Order o1 = new Order(22.3,"Table nr 2");
        Order o2 = new Order(156.4,"Table nr 14");
        Order o3 = new Order(222.3333,"Table nr 21");
        r.addOrder(o1, Arrays.asList(a1,a2,a3));
        r.addOrder(o2, Arrays.asList(a2,a4,a3));
        r.addOrder(o3, Arrays.asList(a7,a6,a5));
        SerializableOperations.writeRestaurantMapToFile(r);
        SerializableOperations.writeItemsToFile(r);

        new GuiFrame();
    }

}
