package restaurant;

import model.MenuItem;
import model.Order;

import java.util.List;

/**
 * Define all the operations available for the restaurant.
 */
public interface RestaurantProc {

    /**
     * Method to add a new menu item to the itemList.
     *
     * @param menuItem
     * @invariant isWellFormed();
     * @pre menuItem != null;
     * @post (getNumberOfItems () != 0) && (getNumberOfItems() == getNumberOfItems()@pre + 1);
     * @invariant isWellFormed();
     */
    void addNewMenuItem(MenuItem menuItem);

    /**
     * Method to delete a menu item from itemList.
     *
     * @param menuItem
     * @invariant isWellFormed();
     * @pre menuItem != null;
     * @post (getNumberOfItems () != 0) && (getNumberOfItems() == getNumberOfItems()@pre - 1);
     * @invariant isWellFormed();
     */
    void deleteMenuItem(MenuItem menuItem);

    /**
     * Method to add a new menu item to the itemList
     *
     * @param menuItem
     * @invariant isWellFormed();
     * @pre menuItem != null
     * @post (getNumberOfItems () > 0) && (getNumberOfItems() == getNumberOfItems()@pre)
     * @invariant isWellFormed()
     */
    void updateMenuItem(MenuItem menuItem);

    /**
     * Method to add a new order with specific menu items to restaurant map;
     *
     * @param order,menuItemList
     * @invariant isWellFormed();
     * @pre order != null && menuItemList!=null && menuItemList.size() > 0;
     * @post (mapSize ! = 0) && (mapSize == mapSize@pre + 1);
     * @invariant isWellFormed();
     */
    void addOrder(Order order, List<MenuItem> menuItemList);

    /**
     * Method to remove a order from restaurant map;
     *
     * @param order
     * @invariant isWellFormed();
     * @pre order != null && order.getId()!=null;
     * @post (mapSize > 0) && (mapSize == mapSize@pre - 1);
     * @invariant isWellFormed();
     */
    void deleteOrder(Order order);

    /**
     * Method to edit existing order from map;
     *
     * @param
     * @invariant isWellFormed();
     * @pre
     * @post
     * @invariant isWellFormed();
     */
    void editOrder();

}
