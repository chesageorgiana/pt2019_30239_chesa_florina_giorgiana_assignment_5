package graphicUserInterface;

import model.MenuItem;
import model.Order;
import serializable.SerializableOperations;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class GuiFrame {

    private JFrame frame;
    private JTable table_Items;
    private JTable table_Orders;
    private JTextField textField_name;
    private JTextField textField_email;
    private JTextField textField_password;
    private JLabel lblUsername;
    private JTextField textField_username;
    private JTextField textField_activity_name;
    private JTextField textField_price;
    private String[] columnNames__Orders = {"ID", "Name", "Type", "Price (Euro)", "Quantity"};
    private String[] columnNames__Items = {"ID", "Table Nr", "Price (Euro)"};

    public GuiFrame() {
        initialize();
        frame.setVisible(true);
    }

    private void initialize() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame = new JFrame();
        frame.setBounds(100, 100, 1262, 565);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        table_Items = new JTable(new DefaultTableModel(null, columnNames__Orders));
        table_Items.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
        table_Items.setColumnSelectionAllowed(true);
        table_Items.setCellSelectionEnabled(true);
        table_Items.setForeground(Color.BLACK);
        table_Items.setFont(new Font("Microsoft JhengHei Light", Font.BOLD, 12));
        table_Items.setBackground(Color.LIGHT_GRAY);

        table_Orders = new JTable(new DefaultTableModel(null, columnNames__Items));
        table_Orders.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
        table_Orders.setColumnSelectionAllowed(true);
        table_Orders.setCellSelectionEnabled(true);
        table_Orders.setForeground(Color.BLACK);
        table_Orders.setFont(new Font("Microsoft JhengHei Light", Font.BOLD, 12));
        table_Orders.setBackground(Color.LIGHT_GRAY);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 13));
        tabbedPane.setBackground(Color.PINK);
        tabbedPane.setBounds(10, 65, 773, 436);
        frame.getContentPane().add(tabbedPane);

        JScrollPane scrollPane = new JScrollPane();
        tabbedPane.addTab("Items", null, scrollPane, null);
        tabbedPane.setForegroundAt(0, Color.RED);
        tabbedPane.setBackgroundAt(0, Color.LIGHT_GRAY);
        scrollPane.setColumnHeaderView(table_Items);
        scrollPane.setViewportView(table_Items);

        JScrollPane scrollPane_1 = new JScrollPane();
        tabbedPane.addTab("Orders", null, scrollPane_1, null);
        scrollPane_1.setColumnHeaderView(table_Orders);
        scrollPane_1.setViewportView(table_Orders);

        refreshTableItem();
        refreshTableOrders();

        JLabel lblNewLabel = new JLabel("Welcome " + " !");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblNewLabel.setBounds(433, 11, 213, 33);
        frame.getContentPane().add(lblNewLabel);

        JButton btnDelete = new JButton("Delete selected item");
        btnDelete.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnDelete.setBounds(422, 55, 174, 23);
        frame.getContentPane().add(btnDelete);

        JLabel lblNewLabel_1 = new JLabel("Add new item");
        lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblNewLabel_1.setForeground(new Color(0, 0, 0));
        lblNewLabel_1.setBounds(893, 218, 91, 33);
        frame.getContentPane().add(lblNewLabel_1);

        JLabel lblName = new JLabel("Name");
        lblName.setForeground(Color.BLACK);
        lblName.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblName.setBounds(793, 255, 91, 33);
        frame.getContentPane().add(lblName);

        JLabel lbType = new JLabel("Type");
        lbType.setForeground(Color.BLACK);
        lbType.setFont(new Font("Tahoma", Font.BOLD, 12));
        lbType.setBounds(793, 299, 91, 33);
        frame.getContentPane().add(lbType);

        JLabel lblQuantity = new JLabel("Quantity");
        lblQuantity.setForeground(Color.BLACK);
        lblQuantity.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblQuantity.setBounds(793, 343, 91, 33);
        frame.getContentPane().add(lblQuantity);

        JButton btnAddNewUser = new JButton("Add new item");
        btnAddNewUser.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnAddNewUser.setBounds(846, 425, 174, 23);
        frame.getContentPane().add(btnAddNewUser);

        textField_name = new JTextField();
        textField_name.setBounds(863, 262, 174, 20);
        frame.getContentPane().add(textField_name);
        textField_name.setColumns(10);

        textField_email = new JTextField();
        textField_email.setColumns(10);
        textField_email.setBounds(863, 306, 174, 20);
        frame.getContentPane().add(textField_email);

        textField_password = new JTextField();
        textField_password.setColumns(10);
        textField_password.setBounds(863, 350, 174, 20);
        frame.getContentPane().add(textField_password);

        lblUsername = new JLabel("Price");
        lblUsername.setForeground(Color.BLACK);
        lblUsername.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblUsername.setBounds(793, 381, 91, 33);
        frame.getContentPane().add(lblUsername);

        textField_username = new JTextField();
        textField_username.setBounds(863, 388, 174, 20);
        frame.getContentPane().add(textField_username);
        textField_username.setColumns(10);

        JButton btnUpdateactivity = new JButton("Update Item");
        btnUpdateactivity.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnUpdateactivity.setBounds(183, 55, 174, 23);
        frame.getContentPane().add(btnUpdateactivity);

        JButton btnGenerateActivitiesReport = new JButton("Generate Bill ");
        btnGenerateActivitiesReport.setFont(new Font("Tahoma", Font.BOLD, 12));
        btnGenerateActivitiesReport.setBounds(903, 132, 253, 23);
        btnGenerateActivitiesReport.addActionListener(e -> generateReport());
        frame.getContentPane().add(btnGenerateActivitiesReport);

        JLabel lblAddNewActivity = new JLabel("Add new order");
        lblAddNewActivity.setForeground(Color.BLACK);
        lblAddNewActivity.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblAddNewActivity.setBounds(1072, 218, 145, 33);
        frame.getContentPane().add(lblAddNewActivity);

        JLabel labelAc = new JLabel("TableNr");
        labelAc.setForeground(Color.BLACK);
        labelAc.setFont(new Font("Tahoma", Font.BOLD, 12));
        labelAc.setBounds(1047, 255, 91, 33);
        frame.getContentPane().add(labelAc);

        JLabel labelPrice = new JLabel("Items");
        labelPrice.setForeground(Color.BLACK);
        labelPrice.setFont(new Font("Tahoma", Font.BOLD, 12));
        labelPrice.setBounds(1036, 299, 91, 33);
        frame.getContentPane().add(labelPrice);

        textField_activity_name = new JTextField();
        textField_activity_name.setBounds(1100, 262, 123, 20);
        frame.getContentPane().add(textField_activity_name);
        textField_activity_name.setColumns(10);

        textField_price = new JTextField();
        textField_price.setColumns(10);
        textField_price.setBounds(1100, 306, 123, 20);
        frame.getContentPane().add(textField_price);

        JButton buttonAddActivity = new JButton("Add new order");
        buttonAddActivity.setFont(new Font("Tahoma", Font.BOLD, 12));
        buttonAddActivity.setBounds(1072, 425, 158, 23);
        frame.getContentPane().add(buttonAddActivity);

        JLabel lblGenerateReportFor = new JLabel("Generate text bill");
        lblGenerateReportFor.setForeground(Color.BLACK);
        lblGenerateReportFor.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblGenerateReportFor.setBounds(964, 88, 253, 33);
        frame.getContentPane().add(lblGenerateReportFor);

        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
    }

    public void refreshTableItem() {
        DefaultTableModel oldTable = (DefaultTableModel) table_Items.getModel();
        oldTable.setRowCount(0);
        List<MenuItem> allItems = new ArrayList<>();
        allItems = SerializableOperations.readItemsFromFile();
        DefaultTableModel newModel = (DefaultTableModel) table_Items.getModel();
        for (int i = 0; i < allItems.size(); i++) {
            newModel.addRow(new Object[]{allItems.get(i).getId(), allItems.get(i).getName(),
                    allItems.get(i).getType(), allItems.get(i).getPrice(),
                    allItems.get(i).getQuantity()});
        }
        table_Items.repaint();
        table_Items.setModel(newModel);
    }

    public void refreshTableOrders() {
        DefaultTableModel oldTable = (DefaultTableModel) table_Orders.getModel();
        oldTable.setRowCount(0);
        List<Order> allOrders = new ArrayList<>(SerializableOperations.readRestaurantFromFile().keySet());
        DefaultTableModel newModel = (DefaultTableModel) table_Orders.getModel();
        for (int i = 0; i < allOrders.size(); i++) {
            newModel.addRow(new Object[]{allOrders.get(i).getId(), allOrders.get(i).getTableNumber(),
                    allOrders.get(i).getPrice()});
        }
        table_Orders.repaint();
        table_Orders.setModel(newModel);
    }

    public void generateReport() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this.frame) == JFileChooser.APPROVE_OPTION) {
            FileWriter fileWritter = null;
            try {
                String activitiesReportText = generateBill();
                fileWritter = new FileWriter(fileChooser.getSelectedFile() + ".txt");
                fileWritter.write(activitiesReportText);
                fileWritter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String generateBill() {
        StringBuilder builder = new StringBuilder();
        Map<Order, List<MenuItem>> all = SerializableOperations.readRestaurantFromFile();
        builder.append("This is a full report of all orders details!\n");
        builder.append("Generated at : ").append(new Date()).append("\n");
        builder.append("There is a total of ").append(all.size()).append(" orders!\n");
        builder.append("\n");
        int activityNr = 1;
        for (List<MenuItem> list : all.values()) {
            builder.append("Number ").append("1").append("\n");
            builder.append("Item: ").append(list.get(activityNr).getName()).append(", ");
            builder.append("Prices: ").append(list.get(activityNr).getPrice()).append(", ");
            builder.append("\n");
            activityNr++;
        }
        return builder.toString();
    }

}
