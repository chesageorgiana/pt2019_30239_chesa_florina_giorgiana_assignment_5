import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * Clasa care modeleaza un dictionar
 * @author Ciceu Gheorghe
 *
 */




@SuppressWarnings("serial")
public class Dictionary implements DictionaryInterface, Serializable{
	
	private HashMap<String,String> dictionar;
	
	/**
	 * Constructorul clasei initializeaza un HashMap
	 */
	public Dictionary(){
		this.dictionar=new HashMap<String,String>();
	}
	@Override
	public void populate() {
		// TODO Auto-generated method stub
		/// citire din fisier linie cu linie  "cuvant = sinonim"
		// separare cuvinte la "="
		// trim pe stringurilo obrinute
		// introducere in dictionar
		BufferedReader  br =null;
		
		try{
			br= new BufferedReader(new FileReader("dictionar.txt"));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String line=new String();
		while(line!=null){
			try {
				line=br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error reading file");
			}
			
			try {
				String cuv=new String();
				String sin= new String();
				cuv+=line.substring(0, line.indexOf("="));
				sin+=line.substring(line.indexOf("=")+1, line.length());
				cuv=cuv.trim();
				sin=sin.trim();
				
				this.dictionar.put(cuv, sin);
			} catch (Exception e) {
				
			}
		}
	}

	
	@Override
	public void add(String word, String definition) {
		assert word!=null;
		assert definition!=null;
		int sz=this.dictionar.size();
		if(this.dictionar.put(word, definition)==null)
			assert this.dictionar.size()==sz+1;
		
	}
	
	@Override
	public boolean remove(String word) {
		assert dictionar.size()>=0 ;
		assert dictionar.containsKey(word);
		int sz= dictionar.size();
		
		boolean ok=false;
		if (this.dictionar.containsKey(word)){
			this.dictionar.remove(word);
			ok= true;
		}
		assert sz == dictionar.size()+1;
		
		return ok;
	}

	

	@Override
	public void save() {
		// salvare in fisier a continutului dictionarului 
		/// golire fisier
		//scris in fisier cheie+" = "+valoare (toate cuvintele din obiect)
		//
		assert this.dictionar.size()>=0;
		assert this.dictionar!=null;
		int sz=this.dictionar.size();
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("dictionar.txt"));
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		try {
			
			writer.write(this.toString());
			writer.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		assert sz==this.dictionar.size();
	}
	
	
	@Override
	public String[] search(String word) {
		//permite expresii ?*   ?-un orice caracter,*-oricate caractere
		
		assert this.dictionar.size()>=0;
		assert this.dictionar!=null;
		int sz=this.dictionar.size();
		ArrayList<String> ar= new ArrayList<>();
		
		String reg=new String();
		
		for(int i=0;i<word.length();i++)
			if(word.charAt(i)=='?')
				reg+="[A-Za-z0-9_]{1}";
		else 
			if(word.charAt(i)=='*')
				reg+="[A-Za-z0-9_]{0,}";
			else
				reg+=word.charAt(i);
		
		for(String s:this.dictionar.keySet()){
			
			if(s.matches(reg))
				ar.add(s+" = "+this.dictionar.get(s)) ;
		}
		
		String[] res=new String[ar.size()];
		int i=0;
		for(String sr:ar){
			res[i]=sr;
			i++;
		}
		assert sz==this.dictionar.size();
		return res;
	
	}
	
	
	@Override
	public boolean copy(String path) {
		
		assert this.dictionar.size()>=0;
		assert this.dictionar!=null;
		int sz=dictionar.size();
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(path));
		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
		
		try {
			
			writer.write(this.toString());
			writer.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		assert sz==this.dictionar.size();
		return true;
	}
	@Override
	public boolean isConsistent() {
		assert this.dictionar.size()>=0;
		int sz=this.dictionar.size();
		
		for(String cuv: this.dictionar.keySet()){
			boolean ok=false;
			for(String sinonim: this.dictionar.values())
				if(sinonim.equals(cuv))
					ok=true;
			if (!ok)
				return false;
				
		}
		
		assert sz==this.dictionar.size();
		return true;
	
	}
	
	public String toString(){
		int sz=this.dictionar.size();
		String s=new String();
		for(String str: this.dictionar.keySet())
			s+=str+" = "+this.dictionar.get(str)+"\n";
		
		assert sz==this.dictionar.size(); 
		return s;
	}
	
	
	/**
	 * Metoda isWellFormed a clasei, verifica daca dictionarul este construit corect
	 * @return true daca dictionarul este construit corect, false in caz contrar
	 */
	public boolean isWellFormed(){
		boolean ok=true;
		
		for(String s : this.dictionar.keySet()){
			if(s==null || this.dictionar.get(s)==null)
				ok = false;
			
		}
		return ok;
	}
	@Override
	public void empty() {
		assert this.dictionar.size()>=0;
		assert this.dictionar != null;
		this.dictionar.clear();
		assert this.dictionar.size()==0;
		
	}
}
