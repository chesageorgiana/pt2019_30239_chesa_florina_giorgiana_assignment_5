/**
 * Interfata pentru clase care modeleaza un dictionar
 * @author Ciceu Gheorghe
 *
 */
public interface DictionaryInterface {
	
	/**
	 * Metoda de populare a dictionarului cu date din fisier
	 */
	public void populate();
	
	/**
	 * metoda de adaugare a unu cuvant in ditionar
	 * @pre word!=null
	 * @post dictionary.size() == dictionary.size@pre+1
	 * @param word cuvantul care se introduce in dictionar
	 * @param sinonim sinonimul cuvantului
	 */
	public void add(String word, String sinonim);
	
	/**
	 * Metoda de stergere a unui cuvant din dictionar
	 * @pre dictionary.containsKey(word);
	 * @pre dictionary.size()>0
	 * @post dictionary.size() == dictionary.size()@pre - 1
	 * @param word cuvantul care se sterge
	 */
	public boolean remove(String word);
	
	/**
	 * @pre dictionary.size()>=0;
	 * @pre dictionary !=null
	 * @post dictionary.size==dictionary.size@pre
	 * Metoda de salvare a continutului dictionarului in fisier
	 */
	public void save();
	
	/**
	 * Metoda de cautare a unui cuvant in dictionar
	 * @pre dictionary.size()>=0
	 * @pre dictionary!=null
	 * @post dictionary.size== dictionary.size@pre
	 * @param word cuvantul sablon dupa care se cauta cuvinte potrivite sablonului
	 * @return un vector cu toate cunvintele care se potrivesc sablonului introdus
	 */
	public String[] search(String word);
	
	/**
	 * Metoda de copiere a dictionarului intrun fisier, cu calea furnizata ca parametru
	 * @pre dictionar.size >=0;
	 * @pre dictionar != null;
	 * @post dictionar.size == dictionar.size@pre
	 * @param path calea spre locul unde se va crea un fisier cu dictionarul
	 */
	public boolean copy(String path);
	
	/**
	 * Metoda care verifica daca toate cuvintele care  sunt sinonime in dictionare au si ele sinonime
	 * @pre dictionary.size>0
	 * @post dictionary.size()@pre == dictionary.size()
	 * @return true daca fiecare sinonim al unui cuvant din dictionar are si el un sinonim
	 */
	public boolean isConsistent();
	
	/**
	 * Metoda care sterge intreg continutul dictionarului
	 * @pre dictionar.size()>0
	 * @pre dictionar !=null
	 * @post dictionar.size=0;
	 */
	public void empty();
	
	/**
	 * Metoda toString a clasei
	 * @post dictionary.size==dictionary.size@pre
	 * @return String cu toate informatiile din dictionar
	 */
	public String toString();
}
