import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
/**
 * Interfata grafica a aplicatiei
 * @author Ciceu Gheorghe
 *
 */

@SuppressWarnings("serial")
public class Interfata extends JFrame {

	
	private JPanel panel;
	private JButton populeaza;
	private Dictionary dictionar;
	private JButton salveaza;
	private JButton adauga;
	private JButton sterge;
	private JButton consistent;
	private JButton search;
	private JPanel searchPan;
	private JTextField tf;
	private JLabel lb;
	private JScrollPane scroller;
	private JTextArea rezultat;
	private JButton ascunde;
	private JButton copy;
	private JButton clear;
	private JButton ser;
	private JButton deser;
	
	public Interfata(){
		super("Dictionar");
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(700, 580);
		this.setVisible(true);
		
		this.pack();
		
		
		dictionar=new Dictionary();
		
		panel= new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0,200, 580);
		panel.setBackground(Color.CYAN);
		this.add(panel);
		
		populeaza = new JButton("Populeaza dictionar");
		populeaza.setBounds(25, 30, 150, 30);
		populeaza.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dictionar.populate();
				JOptionPane.showMessageDialog(null,"Dictionarul a fost populat","",JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		panel.add(populeaza);
		
		salveaza = new JButton("Salveaza continut");
		salveaza.setBounds(25,70,150,30);
		salveaza.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dictionar.save();
				JOptionPane.showMessageDialog(null,"Continutul dictionarului a fost salvat cu succes!","",JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		panel.add(salveaza);
		
		adauga = new JButton("Adauga cuvant");
		adauga.setBounds(25,110,150,30);
		adauga.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String s=null;
				String def=null;
				try{
					s= JOptionPane.showInputDialog(null, "Intorudceti cuvantul:");
				}catch(Exception e1){};
				try{
					def= JOptionPane.showInputDialog(null, "Intorudceti sinonimul cuvantulul:");
				}catch(Exception e2){};
				if(s==null)
					JOptionPane.showMessageDialog(null,
							"Introduceti un cuvant valid!",
							"", 
							JOptionPane.WARNING_MESSAGE);
				else if(def==null)
					JOptionPane.showMessageDialog(null,
							"Introduceti un sinonim!",
							"", 
							JOptionPane.WARNING_MESSAGE);
				else
					dictionar.add(s, def);
				
			}
		});
		panel.add(adauga);
		
		sterge = new JButton("Sterge cuvant");
		sterge.setBounds(25, 150, 150, 30);
		sterge.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String s=null;
				try{
					 s= JOptionPane.showInputDialog(null, "Intorudceti cuvantul care sa fie sters:");
				}catch (Exception e1){
					
				}
				if(s!=null)
					if(!dictionar.remove(s))
						JOptionPane.showMessageDialog(null,
													"Cuvantul nu exista in dictionar!",
													"", 
													JOptionPane.WARNING_MESSAGE);
					else
						JOptionPane.showMessageDialog(null,"Cuvantul \""+s+"\" a fost sters","",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		panel.add(sterge);
		
		consistent = new JButton("Consistent ?");
		consistent.setBounds(25,190,150,30);
		consistent.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (dictionar.isConsistent())
					JOptionPane.showMessageDialog(null,"Dictionarul este consistent","",JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null,"Dictionarul este inconsistent","",JOptionPane.WARNING_MESSAGE);
			}
		});
		panel.add(consistent);
		///////////////////////
		searchPan=new JPanel();
		searchPan.setBounds(200,0,485,400);
		//searchPan.setBackground(Color.BLUE);
		searchPan.setLayout(null);
		searchPan.setVisible(false);
		
		rezultat=new JTextArea();
		scroller = new JScrollPane(rezultat);
		scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroller.setBounds(50, 100, 360, 180);
		scroller.setVisible(true);
		searchPan.add(scroller);
		
		tf = new JTextField();
		tf.setBounds(160, 30, 200, 20);
		tf.addKeyListener(new KeyListener() {
			
			

			@Override
			public void keyTyped(KeyEvent arg0) {
			/*	String s=tf.getText();
				rezultat.setText("");
				String[] v=dictionar.search(s);
				for(int i=0;i<v.length;i++)
					rezultat.append(v[i]+"\n");*/
				
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				String s=new String();
				 s=tf.getText();
				
				rezultat.setText("");
				String[] v=dictionar.search(s);
				for(int i=0;i<v.length;i++)
					rezultat.append(v[i]+"\n");
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				
			}
		});
		searchPan.add(tf);
		
		lb = new JLabel("Scrie cuvantul aici:");
		lb.setBounds(50,30,150,20);
		searchPan.add(lb);
		this.add(searchPan);
		
		search = new JButton("Cautare");
		search.setBounds(25,230,150,30);
		search.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				searchPan.setVisible(true);
				
			}
		});
		
		ascunde = new JButton("Ascunde");
		ascunde.setBounds(160,380,165,20);
		ascunde.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				searchPan.setVisible(false);
				
			}
		});
		searchPan.add(ascunde);
		panel.add(search);
		
		copy = new JButton ("Copiere in fisier");
		copy.setBounds(25,270,150,30);
		copy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String path=null;
				try{
					path=JOptionPane.showInputDialog(null,
							"Intorudceti calea catre locul unde sa fie creat fisierul dictionar"+"\n"+
					"Exemplu: E:\\FACULTATE");
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null,"Introduceti date valide!","",JOptionPane.WARNING_MESSAGE);
					//return;
				}
				if (path == null)
					JOptionPane.showMessageDialog(null,"Introduceti date valide!","",JOptionPane.WARNING_MESSAGE);
				else{
					path+="\\dictionary.txt";
					if(!dictionar.copy(path))
						JOptionPane.showMessageDialog(null,"Nu s-a putut face copierea!","",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		panel.add(copy);
		
		clear = new JButton("Sterge dictionar");
		clear.setBounds(25,310,150,30);
		clear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dictionar.empty();
				JOptionPane.showMessageDialog(null,"Continutul dictionarului a fost sters","",JOptionPane.WARNING_MESSAGE);
				
			}
		});
		panel.add(clear);
		
		////////////////////
		// serializare/ deserializare
		
		ser = new JButton("Serializare");
		ser.setBounds(25, 350,150,30);
		ser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				serialize(dictionar);
				
			}
		});
		panel.add(ser);
		
		deser = new JButton("Deserializare");
		deser.setBounds(25,390,150,30);
		deser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					dictionar=(Dictionary)deserialize();
				}catch(Exception e1){
					e1.printStackTrace();
				}
				
			}
		});
		panel.add(deser);
	}
	public void serialize(Dictionary dictionar){
		try {
			FileOutputStream file=new FileOutputStream("serializat.ser");
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(dictionar);
			
			out.close();
			file.close();
		}catch(IOException i){
		i.printStackTrace();}
		JOptionPane.showMessageDialog(null,"Datele au fost serializate cu succes!","",JOptionPane.INFORMATION_MESSAGE);
	
	}
	public Object deserialize(){
		Object dictionar=0;
		try {
			FileInputStream file=new FileInputStream("serializat.ser");
			ObjectInputStream in = new ObjectInputStream(file);
			
			dictionar =in.readObject();
			in.close();
			file.close();
			
		}
		catch(IOException i){
		i.printStackTrace();
		System.out.println("io eror");
		
		}catch(ClassNotFoundException c){
		c.printStackTrace();
		System.out.println("class not found eror");
		
		}
		JOptionPane.showMessageDialog(null,"Datele au fost deserializate cu succes!","",JOptionPane.INFORMATION_MESSAGE);
		return dictionar;
	}
}
